import express from 'express'
import { Event } from './models'

const app = express()
require('dotenv').config()

app.set('view engine', 'ejs')
app.set('views', 'src/views')
app.use(express.static('public'))

app.get('/', (req, res) => {
  Event.findAll({
    order: [
      ['date', 'ASC'],
    ],
  }).then(
    (event) => res.render('index', { event }),
  ).catch(
    (e) => {
      console.log(e)
      res.status(500).json({ message: e })
    },
  )
})

app.listen(3000, (req, res) => console.log(`Listening on :${process.env.APP_URL}`))
