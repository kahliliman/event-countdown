const moment = require('moment')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Event', [
      {
        id: 'f5110273-abb3-4c77-9828-031b96f61df6',
        name: 'Bayar cicilan DP ke 4',
        date: moment('2021-02-02').format(),
        createdAt: moment().format(),
        updatedAt: moment().format(),
      },
      {
        id: '115c9c91-5d7a-4324-a1be-e47d681f9757',
        name: 'Bayar cicilan DP ke 5',
        date: moment('2021-03-02').format(),
        createdAt: moment().format(),
        updatedAt: moment().format(),
      },
      {
        id: '3074c4b8-63db-4297-8189-57bfa1beac96',
        name: 'Bayar cicilan DP ke 6 (Lunas)',
        date: moment('2021-04-02').format(),
        createdAt: moment().format(),
        updatedAt: moment().format(),
      }], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Event', null, {});
  },
};
